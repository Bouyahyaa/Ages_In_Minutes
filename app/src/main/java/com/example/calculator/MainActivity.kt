package com.example.calculator

import android.app.DatePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val date : Button = findViewById(R.id.btnDatePicker)
        date.setOnClickListener {view ->
            clickDatePicker(view)
        }
    }

    fun clickDatePicker(view : View){
        val myCalendar = Calendar.getInstance()
        val year = myCalendar.get(Calendar.YEAR)
        val month = myCalendar.get(Calendar.MONTH)
        val day = myCalendar.get(Calendar.DAY_OF_MONTH)
        val selectedDateView : TextView = findViewById(R.id.tvSelectedDate)
        val selectedDateViewInMinutes : TextView = findViewById(R.id.tvSelectedDateInMinutes)



        val dpd = DatePickerDialog(this,DatePickerDialog.OnDateSetListener
        { view, selectedYear, selectedMonth, selectedDayOfMonth ->
            Toast.makeText(this,"The chosen year is $selectedYear , the month is $selectedMonth" + "and the day is $selectedDayOfMonth" , Toast.LENGTH_LONG).show()

            val selectedDate = "$selectedDayOfMonth/${selectedMonth+1}/${selectedYear}"

            selectedDateView.setText(selectedDate)

            val sdf = SimpleDateFormat("dd/MM/yyyy",Locale.ENGLISH)

            val theDate = sdf.parse(selectedDate)

            val selectedDateInMinutes = theDate!!.time / 60000

            val currentDate = sdf.parse(sdf.format(System.currentTimeMillis()))

            val currentDateInMinutes = currentDate!!.time / 60000

            val differenceInMinutes = currentDateInMinutes - selectedDateInMinutes

            selectedDateViewInMinutes.setText(differenceInMinutes.toString())
        },
            year,
            month,
            day
        )

        dpd.datePicker.maxDate = Date().time
        dpd.show()

    }
}